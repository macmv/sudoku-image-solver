import cv2
import imutils.contours
import numpy as np
import skimage.transform
import math

def dist(a, b):
  return math.sqrt((a[0] - b[0]) * (a[0] - b[0]) + (a[1] - b[1]) * (a[1] - b[1]))

def mat_transform(p, mat):
  point = np.array([p[0], p[1], 1])
  new_point = np.matmul(point, mat)
  x = int(new_point[0] / new_point[2])
  y = int(new_point[1] / new_point[2])
  return x, y

def inverse_transform(p, mat):
  return mat_transform(p, np.linalg.inv(mat))
  # return mat_transform(p, mat.transpose())

def crop_board(im):
  w = im.shape[0]
  h = im.shape[1]

  gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
  thresh = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 57, 20)

  # Filter out all numbers and noise to isolate only boxes
  cnts = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
  cnts = cnts[0] if len(cnts) == 2 else cnts[1]
  large_cnts = []
  for c in cnts:
    area = cv2.contourArea(c)
    if area > (w / 10) * (h / 10):
      large_cnts.append(c)

  mid = None
  min_dist = 1000000000
  center = None
  for c in large_cnts:
    m = cv2.moments(c)
    if m["m00"] == 0:
      continue
    cx = int(m["m10"] / m["m00"])
    cy = int(m["m01"] / m["m00"])
    d = dist((cx, cy), (w/2, h/2))
    if d < min_dist:
      min_dist = d
      mid = c
      center = (cx, cy)

  top_left  = (int(w/2), int(h/2))
  top_right = (int(w/2), int(h/2))
  bot_left  = (int(w/2), int(h/2))
  bot_right = (int(w/2), int(h/2))

  print("[crop] Found the board contour")

  # This finds the corners of the contour
  for p in mid:
    p = p[0]
    if p[0] < w/2 and p[1] < h/2 and dist(p, center) > dist(top_left, center):
      top_left = tuple(p)
    if p[0] > w/2 and p[1] < h/2 and dist(p, center) > dist(top_right, center):
      top_right = tuple(p)
    if p[0] < w/2 and p[1] > h/2 and dist(p, center) > dist(bot_left, center):
      bot_left = tuple(p)
    if p[0] > w/2 and p[1] > h/2 and dist(p, center) > dist(bot_right, center):
      bot_right = tuple(p)

  print("[crop] Found the corners")

  # Now that we have the corners, we generate a perspective transform to put those corners in the corners of the image.
  # The problem with this is that if the board is at all curved, then we end up cutting off a little bit of it.
  # So, we will use this perspective transform to generate a mesh transform, which will be used to actually transform
  # the image.

  src = np.array([top_left, top_right, bot_left, bot_right], dtype=np.float32)
  dst = np.array([[0, 0], [w, 0], [0, h], [w, h]], dtype=np.float32)
  mat = cv2.getPerspectiveTransform(src, dst)

  # We have out matrix, now we must transform the contour

  new_cnt = []
  mat = mat.transpose()
  for p in mid:
    p = p[0]
    x, y = mat_transform(p, mat)
    new_cnt.append([x, y])

  # Now that we have a transformed contour, we can find which edge each of the points should lie on. Then,
  # we map that to a mesh of points, and transform the image.

  src = []
  dst = []

  print("[crop] Transforming the edges of the board...")

  for p in new_cnt:
    x = p[0]
    y = p[1]
    new_x = None
    new_y = None
    if x < 5:
      new_x = 0
      new_y = y
    elif y < 5:
      new_x = x
      new_y = 0
    elif x > w - 5:
      new_x = w
      new_y = y
    elif y > h - 5:
      new_x = x
      new_y = h
    # Means that the point was not on the edge
    if new_x == None or new_y == None:
      continue
    x, y = inverse_transform((x, y), mat)
    src.append([x, y])
    dst.append([new_x, new_y])

  src = np.array(src)
  dst = np.array(dst)

  mat = skimage.transform.PiecewiseAffineTransform()
  mat.estimate(dst, src)

  print("[crop] Performing mesh deform... (this can take a while)")
  out = skimage.transform.warp(im, mat, output_shape=(h, w))
  print("[crop] Done!")

  return out

def parse_numbers(im):
  w = im.shape[0]
  h = im.shape[1]
  sw = int(w / 9)
  sh = int(h / 9)
  for x in range(9):
    for y in range(9):
      min_x = sw * x
      max_x = sw * (x+1)
      min_y = sh * y
      max_y = sh * (y+1)
      section = im[min_x:max_x, min_y:max_y]
      # TODO: Neural network stuff here
      # This is temporary, just so we can see one of the squares
      return section

if __name__ == '__main__':
  np.set_printoptions(suppress=True)
  base = cv2.imread("images/PXL_20201118_044849434.jpg")
  # base = cv2.imread("images/PXL_20201118_044901784.jpg")
  small = cv2.resize(base, (1000, 1000))

  print("[main] Cropping image to board...")
  board = crop_board(small)
  print("[main] Finding numbers in the board...")
  data = parse_numbers(board)
  cv2.imshow("Board", board)
  cv2.imshow("Section", data)
  cv2.imwrite("images/image.png", board)

  while cv2.waitKey(0) != ord('q'):
    pass
